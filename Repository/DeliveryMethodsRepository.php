<?php

namespace CMS\CatalogBundle\Repository;

use CMS\CoreBundle\AbstractCoreRepository;

/**
 * DeliveryMethodsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DeliveryMethodsRepository extends AbstractCoreRepository
{
    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'CatalogBundle:DeliveryMethods';
    }
}
