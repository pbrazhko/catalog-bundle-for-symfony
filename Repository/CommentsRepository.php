<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 22.04.15
 * Time: 9:55
 */

namespace CMS\CatalogBundle\Repository;


use CMS\CoreBundle\AbstractCoreRepository;

class CommentsRepository extends AbstractCoreRepository {
    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'CatalogBundle:Comments';
    }

}