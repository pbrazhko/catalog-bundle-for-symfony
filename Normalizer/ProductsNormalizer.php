<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 11:24
 */

namespace CMS\CatalogBundle\Normalizer;


use CMS\CatalogBundle\Entity\Products;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class ProductsNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = array())
    {
        $properties = $object->getProperties();

        if($properties instanceof Collection){
            $properties = $properties->toArray();
        }

        /** @var Products $object */
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'price' => $object->getPrice(),
            'purchase_price' => $object->getPurchasePrice(),
            'margin' => $object->getMargin(),
            'currency' => $this->serializer->normalize($object->getCurrency(), $format, $context),
            'properties' => $this->serializer->normalize($properties, $format, $context),
            'is_deleted' => $object->getIsDeleted(),
            'is_published' => $object->getIsPublished(),
            'rating' => $object->getRating()
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Products;
    }

    private function iterator($elements){
        $result = [];

        if(empty($elements)){
            return $result;
        }

        foreach($elements as $element){
            array_push($result, $element->getid());
        }

        return $result;
    }
}