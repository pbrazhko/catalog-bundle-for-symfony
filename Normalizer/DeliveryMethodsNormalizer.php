<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 11:53
 */

namespace CMS\CatalogBundle\Normalizer;


use CMS\CatalogBundle\Entity\DeliveryMethods;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class DeliveryMethodsNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var DeliveryMethods $object */
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'price' => $object->getPrice(),
            'currency' => $this->serializer->normalize($object->getCurrency(), $format, $context)
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof DeliveryMethods;
    }
}