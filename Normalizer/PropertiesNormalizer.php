<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 11:46
 */

namespace CMS\CatalogBundle\Normalizer;


use CMS\CatalogBundle\Entity\Properties;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class PropertiesNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var Properties $object */
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'default_value' => $object->getDefaultValue()
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Properties;
    }
}