<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 11:55
 */

namespace CMS\CatalogBundle\Normalizer;


use CMS\CatalogBundle\Entity\Currencies;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class CurrencyNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var Currencies $object */
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'code' => $object->getCode()
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Currencies;
    }
}