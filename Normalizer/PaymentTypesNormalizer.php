<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 28.09.16
 * Time: 21:35
 */

namespace CMS\CatalogBundle\Normalizer;


use CMS\CatalogBundle\Entity\PaymentTypes;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class PaymentTypesNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof PaymentTypes;
    }
}