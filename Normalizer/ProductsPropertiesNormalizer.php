<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 11:44
 */

namespace CMS\CatalogBundle\Normalizer;


use CMS\CatalogBundle\Entity\ProductsProperties;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class ProductsPropertiesNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var ProductsProperties $object */
        return [
            'id' => $object->getId(),
            'property' => $this->serializer->normalize($object->getProperty(), $format, $context),
            'value' => $object->getValue()
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ProductsProperties;
    }

}