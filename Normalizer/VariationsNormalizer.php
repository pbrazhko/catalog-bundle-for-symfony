<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 24.07.16
 * Time: 20:24
 */

namespace CMS\CatalogBundle\Normalizer;


use CMS\CatalogBundle\Entity\Variations;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class VariationsNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var Variations $object */
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'keywords' => $object->getKeywords(),
            'description' => $object->getDescription(),
            'introduction' => $object->getIntroduction(),
            'content' => $object->getContent(),
            'delivery_methods' => $this->serializer->normalize($object->getDeliveryMethods(), $format, $context),
            'payment_types' => $this->serializer->normalize($object->getPaymentTypes(), $format, $context),
            'products' => $this->serializer->normalize($object->getProducts(), $format, $context),
            'category' => $this->serializer->normalize($object->getCategory(), $format, $context),
            'is_deleted' => $object->getIsDeleted(),
            'is_published' => $object->getIsPublished(),
            'rating' => $object->getRating()
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Variations;
    }
}