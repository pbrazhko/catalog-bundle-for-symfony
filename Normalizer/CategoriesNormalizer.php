<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 11:35
 */

namespace CMS\CatalogBundle\Normalizer;


use CMS\CatalogBundle\Entity\Categories;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class CategoriesNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var Categories $object */
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle()
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Categories;
    }

}