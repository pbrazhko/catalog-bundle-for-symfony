<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 11:49
 */

namespace CMS\CatalogBundle\Normalizer;


use CMS\CatalogBundle\Entity\Comments;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class CommentsNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var Comments $object */
        return [
            'id' => $object->getId(),
            'comment' => $object->getComment(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Comments;
    }
}