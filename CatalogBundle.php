<?php

namespace CMS\CatalogBundle;

use CMS\CatalogBundle\DependencyInjection\Compiler\PaymentTypesCompiler;
use CMS\DashboardBundle\Interfaces\CMSBundleInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CatalogBundle extends Bundle implements CMSBundleInterface
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new PaymentTypesCompiler());
    }

    public function isEnabled()
    {
        return true;
    }

    public function getDescription()
    {
        return [
            [
                'title' => 'Shops',
                'defaultRoute' => 'cms_catalog_shops_list'
            ],
            [
                'title' => 'Payment types',
                'defaultRoute' => 'cms_catalog_payment_types_list'
            ],
            [
                'title' => 'Categories',
                'defaultRoute' => 'cms_catalog_categories_list'
            ],
            [
                'title' => 'Products',
                'defaultRoute' => 'cms_catalog_products_list'
            ],
            [
                'title' => 'Variations',
                'defaultRoute' => 'cms_catalog_variations_list'
            ],
            [
                'title' => 'Currencies',
                'defaultRoute' => 'cms_catalog_currencies_list'
            ],
            [
                'title' => 'Properties',
                'defaultRoute' => 'cms_catalog_properties_list'
            ],
            [
                'title' => 'Delivery methods',
                'defaultRoute' => 'cms_catalog_delivery_methods_list'
            ],
            [
                'title' => 'Orders',
                'defaultRoute' => 'cms_catalog_orders_list'
            ]
        ];
    }

}
