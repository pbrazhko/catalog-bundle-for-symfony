<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 16:15
 */

namespace CMS\CatalogBundle\Twig;


use CMS\CatalogBundle\Entity\Favorites;
use CMS\CatalogBundle\Services\CartService;
use CMS\CatalogBundle\Services\CategoriesService;
use CMS\CatalogBundle\Services\FavoritesService;
use CMS\CatalogBundle\Twig\Options\CategoriesOptions;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig_Environment;

class CatalogTwigExtension extends \Twig_Extension
{
    /**
     * @var CategoriesService
     */
    private $categoriesService;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var FavoritesService
     */
    private $favoritesService;

    /**
     * CategoriesTwigExtension constructor.
     * @param CategoriesService $categoriesService
     * @param CartService $cartService
     * @param FavoritesService $favoritesService
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(CategoriesService $categoriesService, CartService $cartService, FavoritesService $favoritesService, TokenStorageInterface $tokenStorage
    )
    {
        $this->categoriesService = $categoriesService;
        $this->cartService = $cartService;
        $this->favoritesService = $favoritesService;
        $this->tokenStorage = $tokenStorage;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('cms_catalog_categories', [$this, 'getCategories'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('cms_catalog_cart', [$this, 'getCart'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('cms_catalog_in_favorite', [$this, 'isFavorite'])
        ];
    }

    public function getCategories(Twig_Environment $environment, array $options = array())
    {
        $options = CategoriesOptions::resolve($options);

        $criteria = $this->categoriesService->getDefaultsCriteria();

        if(isset($options['parent'])){
            array_push($criteria, ['parent' => $options['parent']]);
        }

        $result = $this->categoriesService->findBy($criteria, [$options['order_by'] => 'desc'], $options['limit']);

        return $environment->render($options['template'], [
            'options' => $options,
            'categories' => $result
        ]);
    }

    public function getCart(\Twig_Environment $environment)
    {
        $cart = $this->cartService->loadCart();

        return $environment->render('CatalogBundle:Twig:cart.html.twig', [
            'cart' => $cart
        ]);
    }

    public function isFavorite(int $variationId)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        if(!$user instanceof UserInterface){
            return false;
        }

        $favorites = $this->favoritesService->getFavorites($user->getId());

        if(!$favorites){
            return false;
        }
        /** @var Favorites $favorite */
        foreach ($favorites as $favorite){
            if($variationId == $favorite->getVariationId()){
                return true;
            }
        }

        return false;
    }

    public function getName()
    {
        return 'cms_catalog_categories_extension';
    }
}