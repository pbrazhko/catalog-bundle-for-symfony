<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 16:38
 */

namespace CMS\CatalogBundle\Twig\Options;


use CMS\CoreBundle\TwigOptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoriesOptions extends TwigOptionsResolver
{
    public static function resolve(array $options = array()){
        $resolver = new OptionsResolver();

        $self = new self();

        $self->configureOptions($resolver);

        return $resolver->resolve($options);
    }

    private function configureOptions(OptionsResolver $resolver){
        $this->setDefined($resolver);
        $this->setDefaults($resolver);
    }

    protected function setDefined(OptionsResolver $resolver){
        $resolver->setDefined([
            'active',
            'parent',
            'order_by',
            'limit',
            'template'
        ]);
    }

    protected function setTypes(OptionsResolver $resolver){
        $resolver->setAllowedTypes('template', 'string');
    }

    protected function setDefaults(OptionsResolver $resolver){
        $resolver->setDefaults([
            'limit' => null,
            'order_by' => 'sort',
            'template' => 'CatalogBundle:Twig:catalog_categories.html.twig'
        ]);
    }
}