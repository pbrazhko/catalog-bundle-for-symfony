<?php

namespace CMS\CatalogBundle\EventListener;

use CMS\CatalogBundle\Entity\Variations;
use CMS\CatalogBundle\Provider\ElasticaVariationsProvider;
use Doctrine\ORM\Event\LifecycleEventArgs;

class VariationsListener
{
    /**
     * @var ElasticaProductsProvider
     */
    private $elasticaVariationsProvider;

    /**
     * ProductsListener constructor.
     * @param ElasticaVariationsProvider $elasticaVariationsProvider
     */
    public function __construct(ElasticaVariationsProvider $elasticaVariationsProvider)
    {
        $this->elasticaVariationsProvider = $elasticaVariationsProvider;
    }

    public function postPersist(LifecycleEventArgs $event){
        $entity = $event->getEntity();

        if(!$entity instanceof Variations){
            return;
        }

        $this->elasticaVariationsProvider->addToIndex($entity->getId());
    }

    public function postUpdate(LifecycleEventArgs $event){
        $entity = $event->getEntity();

        if(!$entity instanceof Variations){
            return;
        }

        if($entity->getIsDeleted()){
            $this->elasticaVariationsProvider->deleteIndex($entity->getId());
            return;
        }

        $this->elasticaVariationsProvider->updateIndex($entity->getId());
    }
}