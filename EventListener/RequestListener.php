<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 10.09.16
 * Time: 20:40
 */

namespace CMS\CatalogBundle\EventListener;


use CMS\CatalogBundle\Services\ViewManagerService;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestListener
{
    /**
     * @var ViewManagerService
     */
    private $productsViewManager;

    /**
     * @var ViewManagerService
     */
    private $categoriesViewManager;

    /**
     * RequestListener constructor.
     * @param ViewManagerService $productsViewManager
     * @param ViewManagerService $categoriesViewManager
     */
    public function __construct(ViewManagerService $productsViewManager, ViewManagerService $categoriesViewManager)
    {
        $this->productsViewManager = $productsViewManager;
        $this->categoriesViewManager = $categoriesViewManager;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event){
        if($event->isMasterRequest()){
            $request = $event->getRequest();

            if($request->isXmlHttpRequest()){
                $this->productsViewManager->setAjaxRequest(true);
                $this->categoriesViewManager->setAjaxRequest(true);
            }
        }
    }
}