<?php

namespace CMS\CatalogBundle\Form;

use CMS\CatalogBundle\Entity\Products;
use CMS\CatalogBundle\Form\Types\ProductsType;
use CMS\CoreBundle\Form\SelectType;
use CMS\GalleryBundle\Form\Types\ImagesType;
use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\LocalizationBundle\Form\Types\LocaleTextareaType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;

class VariationsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('keywords', LocaleTextareaType::class, [
                'required' => false
            ])
            ->add('description', LocaleTextareaType::class, [
                'required' => false
            ])
            ->add('introduction', LocaleTextareaType::class, [
                'required' => false
            ])
            ->add('content', LocaleTextareaType::class, [
                'required' => false
            ])
            ->add('category', LocaleEntityType::class, array(
                'class' => 'CMS\CatalogBundle\Entity\Categories',
                'query_builder' => function (EntityRepository $er) {
                    $builder = $er->createQueryBuilder('c');

                    return
                        $builder
                            ->andWhere(
                                $builder->expr()->isNotNull('c.parent')
                            );
                },
                'group_by' => 'parent.title',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => ''
            ))
            ->add('delivery_methods', LocaleEntityType::class, array(
                'class' => 'CMS\CatalogBundle\Entity\DeliveryMethods',
                'query_builder' => function (EntityRepository $e){
                    return $e->createQueryBuilder('dm')
                        ->andWhere('dm.is_published = 1')
                        ->andWhere('dm.is_deleted = 0');
                },
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'multiple' => true
            ))
            ->add('payment_types', LocaleEntityType::class, array(
                'class' => 'CMS\CatalogBundle\Entity\PaymentTypes',
                'query_builder' => function (EntityRepository $e){
                    return $e->createQueryBuilder('pm')
                        ->andWhere('pm.is_deleted = 0');
                },
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'multiple' => true
            ))
            ->add('is_published', CheckboxType::class, [
                'data' => true
            ])
            ->add('is_deleted')
            ->add('sort', NumberType::class, [
                'scale' => 0,
                'data' => 0
            ])
            ->add('rating', NumberType::class, [
                'scale' => 1,
                'data' => 1.0
            ])
            ->add('products', SelectType::class, [
                'additional_properties' => [
                    'price' => 'price',
                    'currency' => 'currency.code',
                    'properties' => ''
                ],
                'icon_delete_class' => 'glyphicon glyphicon-remove',
                'attr' => [
                    'class' => 'table table-hover'
                ],
                'constraints' => [
                    new Count([
                        'min' => 1
                    ])
                ]
            ])
            ->add('related_variations', LocaleEntityType::class, array(
                'class' => 'CMS\CatalogBundle\Entity\Variations',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'multiple' => true,
                'required' => false
            ))
            ->add('images', ImagesType::class, [
                'constraints' => [
                    new Count([
                        'min' => 1
                    ])
                ]
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\CatalogBundle\Entity\Variations',
            'translation_domain' => 'catalog'
        ));
    }
}
