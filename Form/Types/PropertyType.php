<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.04.15
 * Time: 11:38
 */

namespace CMS\CatalogBundle\Form\Types;


use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PropertyType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('property', LocaleEntityType::class, array(
                'class' => 'CMS\CatalogBundle\Entity\Properties',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => ''
            ))
            ->add('value')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(array(
            'data_class' => 'CMS\CatalogBundle\Entity\ProductsProperties'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_catalog_property_type';
    }
}