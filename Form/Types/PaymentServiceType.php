<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 25.09.16
 * Time: 16:32
 */

namespace CMS\CatalogBundle\Form\Types;

use CMS\CatalogBundle\Services\PaymentTypesRegistry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentServiceType extends AbstractType
{

    /**
     * @var array
     */
    private $choices;

    /**
     * PaymentServiceType constructor.
     * @param PaymentTypesRegistry $paymentTypesRegistry
     */
    public function __construct(PaymentTypesRegistry $paymentTypesRegistry)
    {
        $paymentTypes = $paymentTypesRegistry->getPaymentTypes();
        $this->choices = [];

        foreach ($paymentTypes as $nameType => $paymentType){
            $this->choices[$paymentType->getName()] = $nameType;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => $this->choices
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

}