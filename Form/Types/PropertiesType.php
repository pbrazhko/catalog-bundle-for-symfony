<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.04.15
 * Time: 11:35
 */

namespace CMS\CatalogBundle\Form\Types;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PropertiesType extends AbstractType{
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(array(
            'entry_type' => PropertyType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'delete_empty' => true,
            'by_reference' => true
        ));
    }

    public function getParent()
    {
        return CollectionType::class;
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_catalog_properties_type';
    }
}