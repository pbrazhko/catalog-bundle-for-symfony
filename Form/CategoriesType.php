<?php

namespace CMS\CatalogBundle\Form;

use CMS\GalleryBundle\Form\Types\ImagesType;
use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\LocalizationBundle\Form\Types\LocaleTextareaType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoriesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('keywords', LocaleTextareaType::class)
            ->add('description', LocaleTextareaType::class)
            ->add('is_published')
            ->add('is_deleted')
            ->add('sort')
            ->add('parent', LocaleEntityType::class, array(
                'class' => 'CatalogBundle:Categories',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'required' => false
            ))
            ->add('shop', LocaleEntityType::class, array(
                'class' => 'CatalogBundle:Shops',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
            ))
            ->add('images', ImagesType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\CatalogBundle\Entity\Categories'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_catalogbundle_categories';
    }
}
