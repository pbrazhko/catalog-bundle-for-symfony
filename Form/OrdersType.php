<?php

namespace CMS\CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrdersType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('customer')
            ->add('customerPhone')
            ->add('customerEmail')
            ->add('deliveryAddress')
            ->add('totalPrice')
            ->add('status')
            ->add('is_deleted')
            ->add('date_create')
            ->add('date_update')
            ->add('create_by')
            ->add('update_by')
            ->add('deliveryMethod')
            ->add('pamentType')
            ->add('currency')        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\CatalogBundle\Entity\Orders'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'cms_catalogbundle_orders';
    }


}
