<?php

namespace CMS\CatalogBundle\Form;

use CMS\LocalizationBundle\Form\Types\LocaleTextareaType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('address', LocaleTextareaType::class)
            ->add('addressRegistration', LocaleTextareaType::class)
            ->add('email')
            ->add('phone')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\CatalogBundle\Entity\Shops',
            'translation_domain' => 'labels'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_catalogbundle_shops';
    }
}
