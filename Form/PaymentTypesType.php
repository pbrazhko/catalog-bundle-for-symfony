<?php

namespace CMS\CatalogBundle\Form;

use CMS\CatalogBundle\Form\Types\PaymentServiceType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentTypesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('paymentService', PaymentServiceType::class)
            ->add('is_deleted')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\CatalogBundle\Entity\PaymentTypes'
        ));
    }
}
