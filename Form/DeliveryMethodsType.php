<?php

namespace CMS\CatalogBundle\Form;

use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeliveryMethodsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('price')
            ->add('currency', LocaleEntityType::class, [
                'class' => 'CMS\CatalogBundle\Entity\Currencies',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => ''
            ])
            ->add('time_delivery')
            ->add('max_time_delivery')
            ->add('address_required')
            ->add('is_published')
            ->add('is_deleted')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\CatalogBundle\Entity\DeliveryMethods',
            'translation_domain' => 'label'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_catalogbundle_deliverymethods';
    }
}
