<?php

namespace CMS\CatalogBundle\Form;

use CMS\CatalogBundle\Form\Types\PropertiesType;
use CMS\GalleryBundle\Form\Types\ImagesType;
use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Count;

class ProductsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('purchase_price')
            ->add('margin')
            ->add('price', TextType::class, [
                'disabled' => true
            ])
            ->add('currency', LocaleEntityType::class, array(
                'class' => 'CMS\CatalogBundle\Entity\Currencies',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => ''
            ))
            ->add('is_published', CheckboxType::class, [
                'data' => true
            ])
            ->add('is_deleted')
            ->add('sort', NumberType::class, [
                'scale' => 0,
                'data' => 0
            ])
            ->add('rating', NumberType::class, [
                'scale' => 1,
                'data' => 1.0
            ])
            ->add('properties', PropertiesType::class, [
                'constraints' => [
                    new Count([
                        'min' => 1
                    ])
                ]
            ])
            ->add('variations', LocaleEntityType::class, array(
                'class' => 'CMS\CatalogBundle\Entity\Variations',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'required' => false,
                'multiple' => true
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\CatalogBundle\Entity\Products'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_catalogbundle_products';
    }
}
