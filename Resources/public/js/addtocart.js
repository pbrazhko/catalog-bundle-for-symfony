/**
 * Created by pavel on 25.10.16.
 */
(function($) {
    var addtocart = {};

    addtocart.config = {
        'addtocart_button_selector': null,
        'route_add': null
    };

    addtocart.impl = {
        cartSelector: null,
        config: {},
        init: function (cartSelector, config){
            this.cartSelector = cartSelector;
            this.config = $.extend({}, addtocart.config, config);

            if(!this.configValidation()){
                return;
            }

            this.bind();
        },
        add: function(productId) {
            var self = this;

            $.ajax({
                type: 'POST',
                url: Routing.generate(this.config.route_add, {'id': productId}),
                dataType: 'json',
                success: function(data) {
                    if(data.status == 'success'){
                        $(self.cartSelector).html(data.data);
                    }

                    $(self.cartSelector).tooltip({
                        animation: true,
                        placement: 'bottom',
                        html: true,
                        title: data.tooltip
                    });

                    $(self.cartSelector).tooltip('show');

                    setTimeout(function() {
                        $(self.cartSelector).tooltip('destroy');
                    }, 4000);
                }
            })
        },
        checkout: function (reference){
            var form = $(reference).closest('form');
            $(form).attr('action', '/cart/order');
            $(form).submit();
        },
        bind: function(){
            var self = this;
            $(document).on('click', this.config.addtocart_button_selector, function(e){
                e.preventDefault();

                self.add($(this).attr('data-product'));
            });
        },
        configValidation: function(){
            for(var parameter in this.config){
                if(this.config[parameter] == undefined || this.config[parameter] == null){
                    throw new Error(parameter + 'is required');
                    return false;
                }
            }

            return true;
        }
    };

    $.fn.addtocart = function (config){
        return addtocart.impl.init(this, config);
    };
})(jQuery);