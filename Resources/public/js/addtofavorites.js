/**
 * Created by pavel on 25.10.16.
 */
(function($) {
    var addtofavorites = {};

    addtofavorites.config = {
        'addtofavorites_button_selector': null,
        'route_add': null,
        'route_remove': null
    };

    addtofavorites.impl = {
        config: {},
        init: function (config){
            this.config = $.extend({}, addtofavorites.config, config);

            if(!this.configValidation()){
                return;
            }

            this.bind();
        },
        add: function(selector, variationId) {
            var self = this;

            $.ajax({
                type: 'POST',
                url: Routing.generate(this.config.route_add, {'id': variationId}),
                dataType: 'json',
                success: function(data) {
                    if(data.status == 'success'){
                        $(selector)
                            .unbind('click')
                            .addClass('added')
                            .on('click', function (e){
                                self.remove(selector, variationId)
                            })
                        ;
                    }
                },
                error: function (response){
                    var data = jQuery.parseJSON(response.responseText);
                    self.showMessage(selector, data.message);
                }
            })
        },
        remove: function (selector, variationId) {
            var self = this;

            $.ajax({
                type: 'DELETE',
                url: Routing.generate(this.config.route_remove, {'id': variationId}),
                dataType: 'json',
                success: function(data) {
                    if(data.status == 'success'){
                        $(selector)
                            .unbind('click')
                            .removeClass('added')
                            .on('click', function (e){
                                self.add(selector, variationId)
                            })
                        ;
                    }
                },
                error: function (response){
                    console.log(response.getData());
                }
            })
        },
        showMessage: function(selector, message){
            $(selector).tooltip({
                animation: true,
                placement: 'bottom',
                html: true,
                title: message
            });

            $(selector).tooltip('show');

            setTimeout(function() {
                $(selector).tooltip('destroy');
            }, 4000);
        },
        bind: function(){
            var self = this;
            var buttons = $(this.config.addtofavorites_button_selector);

            buttons.each(function (index, button){
               $(button).on('click', function(e){
                   e.preventDefault();

                   var variationId = $(this).attr('data-variation');
                   if($(button).hasClass('added')){
                       self.remove(this, variationId);
                   }
                   else{
                       self.add(this, variationId);
                   }
               });
            });
        },
        configValidation: function(){
            for(var parameter in this.config){
                if(this.config[parameter] == undefined || this.config[parameter] == null){
                    throw new Error(parameter + 'is required');
                    return false;
                }
            }

            return true;
        }
    };

    $.fn.addtofavorites = function (config){
        return addtofavorites.impl.init(config);
    };
})(jQuery);