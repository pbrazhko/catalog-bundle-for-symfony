/**
 * Created by pavel on 23.04.15.
 */

$(document).ready(function () {
    var propertiesPrototype = $('#properties').data('prototype');
    var propertiesIndex = $('#properties').find('.property').length;

    function closeProperty(selector) {
        $(selector).closest('.property').hide("puff", function () {
            $(this).remove();
        });
    }

    $('#add_property').on('click', function (event) {
        propertiesIndex = propertiesIndex + 1;

        var newProperty = $.parseHTML(propertiesPrototype.replace(/__name__/g, propertiesIndex));

        $('#properties').append(newProperty);

        $(newProperty).find('.close').on('click', function (event) {
            event.preventDefault();

            closeProperty(this);
        });
    });

    $('.property .close').on('click', function (event) {
        event.preventDefault();

        closeProperty(this);
    });
});
