/**
 * Created by pavel on 25.10.16.
 */
(function($) {
    var cart = {};

    cart.config = {
        'loader_selector': null,
        'route_delete': null,
        'route_reduce': null,
        'route_increase': null,
        'route_change_count': null,
        'count_selector': null,
        'product_selector': null,
        'product_total_selector': null,
        'cart_total_selector': null,
        'button_increase_selector': null,
        'button_reduce_selector': null,
        'button_delete_selector': null
    };

    cart.impl = {
        cartSelector: null,
        totalSumSelector: null,
        config: {},
        changed: true,
        init: function (cartSelector, config){
            this.cartSelector = cartSelector;
            this.config = $.extend({}, cart.config, config);

            if(!this.configValidation()) return;

            this.bind();
        },
        upCount: function (selector){
            if(!this.allowedChangeCount(selector, 'up')) return;

            this.send(this.config.route_increase, {'id': this.getProductId(selector)}, this.changeCountCallback, selector);
        },
        downCount: function (selector){
            if(!this.allowedChangeCount(selector, 'down')) return;

            this.send(this.config.route_reduce, {'id': this.getProductId(selector)}, this.changeCountCallback, selector);
        },
        changeCount: function (e, selector){
            var count = $(selector).val();

            if(!this.changed || !$.isNumeric(count) || !this.allowedChangeCount(selector)){
                return false;
            }

            this.send(this.config.route_change_count, {'id': this.getProductId(selector), 'count': count}, this.changeCountCallback, selector);
        },
        remove: function (selector){
            var self = this;

            var callback = function (cart, result, selector){
                if(result.status != 'success') return;

                $(selector).closest(self.config.product_selector).remove();
                self.updateCartTotal(result.total);

                if($(self.config.product_selector).length < 1){
                    window.location.reload();
                }
            };

            this.send(this.config.route_delete, {'id': this.getProductId(selector)}, callback, selector);
        },
        updateInfo: function (selector, product) {
            $(selector)
                .closest(this.config.product_selector)
                .find(this.config.count_selector)
                .val(product.count);

            $(selector)
                .closest(this.config.product_selector)
                .find(this.config.product_total_selector)
                .text(product.product_total_price + ' ' + product.currency);
        },
        updateCartTotal: function (total){
            var totalText = '';

            for (var currency in total){
                var sum = total[currency];
                totalText += sum + ' ' + currency;
            }

            $(this.config.cart_total_selector).html(totalText);
        },
        allowedChangeCount: function(selector, action){
            var value = $(selector)
                .closest(this.config.product_selector)
                .find(this.config.count_selector)
                .val();

            if(parseInt(value) < 1 && action == undefined){
                return false;
            }
            else if(parseInt(value) < 2 && action == 'down'){
                return false;
            }

            return true;
        },
        getProductId: function (selector){
            return $(selector).closest(this.config.product_selector).data('product');
        },
        changeCountCallback: function (cart, result, selector){
            if(result.status != 'success') return;

            cart.updateInfo(selector, result.product);
            cart.updateCartTotal(result.total);
        },
        bind: function(){
            var self = this;
            $(this.config.button_increase_selector).click(function(e){
                e.preventDefault();

                self.upCount(this);
            });

            $(this.config.button_reduce_selector).click(function(e){
                e.preventDefault();

                self.downCount(this);
            });

            $(this.config.button_delete_selector).click(function(e){
                e.preventDefault();

                self.remove(this);
            });

            $(this.config.count_selector).keydown(function(e){
                self.changed = true;

                if(e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    self.changed = false;
                    e.preventDefault();
                }
            });

            $(this.config.count_selector).keyup(function (e){
                self.changeCount(e, this);
            });
        },
        configValidation: function(){
            for(var parameter in this.config){
                if(this.config[parameter] == undefined || this.config[parameter] == null){
                    throw new Error(parameter + ' is required');
                }
            }

            return true;
        },
        send: function (route, routeParameters, callback, callbackArgs){
            var cart = this;

            $(this.config.loader_selector).addClass('show');

            $.ajax({
                'type': 'POST',
                'url': Routing.generate(route, routeParameters),
                'dataType': 'json',
                'success': function (result){
                    $(cart.config.loader_selector).removeClass('show');

                    if($.isFunction(callback)) callback(cart, result, callbackArgs);
                }
            })
            .fail(function (){})
        }
    };

    $.fn.cart = function (config){
        return cart.impl.init(this, config);
    };
})(jQuery);