<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 25.09.16
 * Time: 15:41
 */

namespace CMS\CatalogBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class PaymentTypesCompiler implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if(!$container->hasDefinition('cms.catalog.payment_types.registry')){
            return;
        }

        $paymentTypesService = $container->getDefinition('cms.catalog.payment_types.registry');

        $paymentTypesDefinition = $container->findTaggedServiceIds('payment_type');

        foreach ($paymentTypesDefinition as $id => $tags){
            foreach ($tags as $attributes){
                if(!isset($attributes['alias'])){
                    continue;
                }

                $paymentTypesService->addMethodCall('addPaymentType', [$attributes['alias'], new Reference($id)]);
            }
        }
    }
}