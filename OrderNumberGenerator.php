<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 18.11.16
 * Time: 20:53
 */

namespace CMS\CatalogBundle;


class OrderNumberGenerator
{
    private static $source = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    public static function generate(string $prefix = 'ORD'){

        list($milliseconds, $timestamp) = explode(' ', microtime());
        $numberString = preg_replace('/[\.\s]/', '', $milliseconds) . date('dmy', $timestamp);

        $hash = '';

        $length = strlen($numberString);

        for($i = 4; $i < $length; $i=$i+2){
            $part = substr($numberString, $i-2, 2);
            $index = ((int) array_sum(str_split($part)))*2;

            $hash .= $numberString[abs($length-$i)] . self::$source[$index==36?$index-1:$index];
        }

        return $prefix . '-' . $hash;
    }
}