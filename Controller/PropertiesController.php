<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 22.04.15
 * Time: 12:50
 */

namespace CMS\CatalogBundle\Controller;

use CMS\CatalogBundle\Entity\Properties;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PropertiesController
 * @package CMS\CatalogBundle\Controller
 */
class PropertiesController extends Controller{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(){
        $service = $this->get('cms.catalog.properties.service');

        return $this->render('CatalogBundle:Properties:list.html.twig', array(
            'properties' => $service->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request){
        $service = $this->get('cms.catalog.properties.service');

        $form = $service->generateForm();

        if($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var Properties $data */
                $data = $form->getData();

                $data->setDateCreate(new \DateTime());
                $data->setDateUpdate(new \DateTime());
                $data->setCreateBy($this->getUser()->getId());
                $data->setUpdateBy($this->getUser()->getId());

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_catalog_properties_list'));
            }
        }

        return $this->render('CatalogBundle:Properties:edit.html.twig', array(
            'form' =>$form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id){
        $service = $this->get('cms.catalog.properties.service');

        $form = $service->generateForm($service->findOneBy(['id' => $id]));

        if($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var Properties $data */
                $data = $form->getData();

                $data->setDateCreate(new \DateTime());
                $data->setDateUpdate(new \DateTime());
                $data->setCreateBy($this->getUser()->getId());
                $data->setUpdateBy($this->getUser()->getId());

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_catalog_properties_list'));
            }
        }

        return $this->render('CatalogBundle:Properties:edit.html.twig', array(
            'form' =>$form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id){
        $service = $this->get('cms.catalog.properties.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_catalog_properties_list'));
    }
}