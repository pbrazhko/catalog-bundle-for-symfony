<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 01.07.16
 * Time: 17:34
 */

namespace CMS\CatalogBundle\Controller;


use CMS\CatalogBundle\Entity\Variations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class VariationsController extends Controller
{
    public function listAction(Request $request, $offset = 0, $limit= 20){
        $offset > 0 ?: $offset = 0;
        $limit > 0 ?: $limit = 20;
        
        $variationsService = $this->get('cms.catalog.variations.service');
        
        return $this->render('CatalogBundle:Variations:list.html.twig', [
            'variations' => $variationsService->findBy(['is_deleted' => false], ['date_create' => 'desc'], $limit+1, $offset),
            'offset' => $offset,
            'limit' => $limit
        ]);
    }
    
    public function createAction(Request $request){
        $service = $this->get('cms.catalog.variations.service');

        $form = $service->generateForm();

        if($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var Variations $data */
                $data = $form->getData();
                
                $service->create($data);

                return $this->redirect($this->generateUrl('cms_catalog_variations_list'));
            }
        }

        return $this->render('CatalogBundle:Variations:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $id){
        $service = $this->get('cms.catalog.variations.service');

        $form = $service->generateForm($service->findOneBy(['id' => $id]));

        if($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var Variations $data */
                $data = $form->getData();

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_catalog_variations_list'));
            }
        }

        return $this->render('CatalogBundle:Variations:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction($id){
        $service = $this->get('cms.catalog.variations.service');

        /** @var Variations $variation */
        if(null === ($variation = $service->findOneBy(['id' => $id]))){
            throw $this->createNotFoundException('Variation not found!');
        }

        $variation->setIsDeleted(true);

        $service->update($variation);

        return $this->redirect($this->generateUrl('cms_catalog_variations_list'));
    }
}