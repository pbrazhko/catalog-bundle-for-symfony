<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 25.09.16
 * Time: 15:55
 */

namespace CMS\CatalogBundle\Controller;


use CMS\CatalogBundle\Entity\PaymentTypes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PaymentTypesController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(){
        return $this->render('CatalogBundle:PaymentTypes:list.html.twig', array(
            'payment_types' => $this->get('cms.catalog.payment_types.service')->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request){
        $service = $this->get('cms.catalog.payment_types.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')){
            $form->handleRequest($request);

            if ($form->isValid()){
                /** @var PaymentTypes $data */
                $data = $form->getData();

                $data->setDateUpdate(new \DateTime());
                $data->setDateCreate(new \DateTime());
                $data->setCreateBy($this->getUser()->getId());
                $data->setUpdateBy($this->getUser()->getId());

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_catalog_payment_types_list'));
            }
        }

        return $this->render('CatalogBundle:PaymentTypes:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id){
        $service = $this->get('cms.catalog.payment_types.service');

        $form = $service->generateForm($service->findOneBy(['id' => $id]));

        if ($request->isMethod('POST')){
            $form->handleRequest($request);

            if ($form->isValid()){
                /** @var PaymentTypes $data */
                $data = $form->getData();

                $data->setDateUpdate(new \DateTime());
                $data->setCreateBy($this->getUser()->getId());

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_catalog_payment_types_list'));
            }
        }

        return $this->render('CatalogBundle:PaymentTypes:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id){
        $service = $this->get('cms.catalog.payment_types.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_catalog_payment_types_list'));
    }
}