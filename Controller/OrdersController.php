<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 30.10.16
 * Time: 15:54
 */

namespace CMS\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OrdersController extends Controller
{
    public function listAction()
    {
        $service = $this->get('cms.catalog.orders.service');

        return $this->render('CatalogBundle:Orders:list.html.twig', array(
            'orders' => $service->findAll()
        ));
    }
}