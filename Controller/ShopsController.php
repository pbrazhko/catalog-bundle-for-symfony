<?php

namespace CMS\CatalogBundle\Controller;

use CMS\CatalogBundle\Entity\Shops;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ShopsController
 * @package CMS\CatalogBundle\Controller
 */
class ShopsController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $shopsService = $this->get('cms.catalog.shops.service');

        return $this->render('CatalogBundle:Shops:list.html.twig', array(
            'shops' => $shopsService->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request){

        $service = $this->get('cms.catalog.shops.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')){
            $form->handleRequest($request);

            if ($form->isValid()){
                /** @var Shops $data */
                $data = $form->getData();

                $data->setDateUpdate(new \DateTime());
                $data->setDateCreate(new \DateTime());
                $data->setCreateBy($this->getUser()->getId());
                $data->setUpdateBy($this->getUser()->getId());

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_catalog_shops_list'));
            }
        }

        return $this->render('CatalogBundle:Shops:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id){
        $service = $this->get('cms.catalog.shops.service');

        $form = $service->generateForm($service->findOneBy(['id' => $id]));

        if ($request->isMethod('POST')){
            $form->handleRequest($request);

            if ($form->isValid()){
                /** @var Shops $data */
                $data = $form->getData();

                $data->setDateUpdate(new \DateTime());
                $data->setUpdateBy($this->getUser()->getId());

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_catalog_shops_list'));
            }
        }

        return $this->render('CatalogBundle:Shops:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id){
        $service = $this->get('cms.catalog.shops.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_catalog_shops_list'));
    }
}
