<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.04.15
 * Time: 11:17
 */

namespace CMS\CatalogBundle\Controller;


use CMS\CatalogBundle\Entity\Products;
use CMS\CatalogBundle\Normalizer\CurrencyNormalizer;
use CMS\CatalogBundle\Normalizer\ProductsNormalizer;
use CMS\CatalogBundle\Normalizer\ProductsPropertiesNormalizer;
use CMS\CatalogBundle\Normalizer\PropertiesNormalizer;
use CMS\CatalogBundle\Repository\ProductsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ProductsController
 * @package CMS\CatalogBundle\Controller
 */
class ProductsController extends Controller{
    /**
     * @param Request $request
     * @param int $offset
     * @param int $limit
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request, $offset = 0, $limit = 20){
        $offset > 0 ?: $offset = 0;
        $limit > 0 ?: $limit = 20;

        $service = $this->get('cms.catalog.products.service');

        return $this->render('CatalogBundle:Products:list.html.twig', array(
            'products' => $service->findBy([], ['sort' => 'desc'], $limit+1, $offset),
            'offset' => $offset,
            'limit' => $limit
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request){
        $service = $this->get('cms.catalog.products.service');

        $form = $service->generateForm();

        if($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var Products $data */
                $data = $form->getData();

                $data->setPrice($this->calcPrice($data));
                $data->setDateCreate(new \DateTime());
                $data->setDateUpdate(new \DateTime());
                $data->setCreateBy($this->getUser()->getId());
                $data->setUpdateBy($this->getUser()->getId());

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_catalog_products_list'));
            }
        }

        return $this->render('CatalogBundle:Products:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id){
        $service = $this->get('cms.catalog.products.service');

        $form = $service->generateForm($service->findOneBy(['id' => $id]));

        if($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var Products $data */
                $data = $form->getData();

                $data->setPrice($this->calcPrice($data));

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_catalog_products_list'));
            }
        }

        return $this->render('CatalogBundle:Products:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id){
        $service = $this->get('cms.catalog.products.service');

        /** @var Products $product */
        if(null === ($product = $service->findOneBy(['id' => $id]))){
            throw $this->createNotFoundException('Product not found!');
        }

        $product->setIsDeleted(true);

        $service->update($product);

        return $this->redirect($this->generateUrl('cms_catalog_products_list'));
    }

    /**
     * @param Request $request
     * @param $locale
     * @param $query
     * @param $limit
     * @return Response
     */
    public function findProductsAction(Request $request, $locale, $query, $limit){
        if(!$request->isXmlHttpRequest()){
            throw $this->createNotFoundException('Page not found!');
        }
        
        $service = $this->get('cms.catalog.products.service');
        /** @var ProductsRepository $productsRepository */
        $productsRepository = $service->getRepository();
        
        $serializer = new Serializer([
                new ProductsNormalizer(),
                new ProductsPropertiesNormalizer(),
                new CurrencyNormalizer(),
                new PropertiesNormalizer()
            ],
            [new JsonEncoder()]);
        
        return new Response(
            $serializer->serialize(
                $productsRepository->findProduct($query, $limit),
                'json'
            )
        );
    }

    private function calcPrice(Products $product)
    {
        return $product->getPurchasePrice() + $product->getPurchasePrice() *($product->getMargin()/100);
    }
}