<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.04.15
 * Time: 17:49
 */

namespace CMS\CatalogBundle\Controller;


use CMS\CatalogBundle\Entity\Categories;
use CMS\CatalogBundle\Entity\Currencies;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CurrenciesController extends Controller{
    public function listAction(){
        $service = $this->get('cms.catalog.currencies.service');

        return $this->render('CatalogBundle:Currencies:list.html.twig', array(
            'currencies' => $service->findAll()
        ));
    }

    public function createAction(Request $request){
        $service = $this->get('cms.catalog.currencies.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var Currencies $data */
                $data = $form->getData();
                $data->setDateCreate(new \DateTime());
                $data->setDateUpdate(new \DateTime());
                $data->setCreateBy($this->getUser()->getId());
                $data->setUpdateBy($this->getUser()->getId());

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_catalog_currencies_list'));
            }
        }

        return $this->render('CatalogBundle:Currencies:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $id){
        $service = $this->get('cms.catalog.currencies.service');

        $form = $service->generateForm($service->findOneBy(['id' => $id]));

        if ($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var Categories $data */
                $data = $form->getData();

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_catalog_currencies_list'));
            }
        }

        return $this->render('CatalogBundle:Currencies:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id){
        $service = $this->get('cms.catalog.currencies.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_catalog_currencies_list'));
    }
}