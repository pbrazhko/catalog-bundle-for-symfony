<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 25.04.2015
 * Time: 13:01
 */

namespace CMS\CatalogBundle\Controller;


use CMS\CatalogBundle\Entity\DeliveryMethods;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DeliveryMethodsController extends Controller{
    public function listAction(){
        $service = $this->get('cms.catalog.delivery_methods.service');

        return $this->render('CatalogBundle:DeliveryMethods:list.html.twig', array(
            'deliveryMethods' => $service->findAll()
        ));
    }

    public function createAction(Request $request){
        $service = $this->get('cms.catalog.delivery_methods.service');

        $form = $service->generateForm();

        if($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var DeliveryMethods $data */
                $data = $form->getData();

                $data->setDateCreate(new \DateTime());
                $data->setDateUpdate(new \DateTime());
                $data->setCreateBy($this->getUser()->getId());
                $data->setUpdateBy($this->getUser()->getId());

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_catalog_delivery_methods_list'));
            }
        }

        return $this->render('CatalogBundle:DeliveryMethods:edit.html.twig',array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $id){
        $service = $this->get('cms.catalog.delivery_methods.service');

        $form = $service->generateForm($service->findOneBy(['id' => $id]));

        if($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var DeliveryMethods $data */
                $data = $form->getData();

                $data->setDateCreate(new \DateTime());
                $data->setDateUpdate(new \DateTime());
                $data->setCreateBy($this->getUser()->getId());
                $data->setUpdateBy($this->getUser()->getId());

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_catalog_delivery_methods_list'));
            }
        }

        return $this->render('CatalogBundle:DeliveryMethods:edit.html.twig',array(
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id){
        $service = $this->get('cms.catalog.delivery_methods.service');

        /** @var DeliveryMethods $deliveryMethod */
        if(null === ($deliveryMethod = $service->findOneBy(['id' => $id]))){
            throw $this->createNotFoundException('Delivery method not found!');
        }

        $deliveryMethod->setIsDeleted(true);

        $service->update($deliveryMethod);

        return $this->redirect($this->generateUrl('cms_catalog_delivery_methods_list'));
    }
}