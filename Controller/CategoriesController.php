<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.04.15
 * Time: 17:49
 */

namespace CMS\CatalogBundle\Controller;


use CMS\CatalogBundle\Entity\Categories;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CategoriesController extends Controller{
    public function listAction(){
        $service = $this->get('cms.catalog.categories.service');

        return $this->render('CatalogBundle:Categories:list.html.twig', array(
            'categories' => $service->findAll()
        ));
    }

    public function createAction(Request $request){
        $service = $this->get('cms.catalog.categories.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var Categories $data */
                $data = $form->getData();

                $data->setDateCreate(new \DateTime());
                $data->setDateUpdate(new \DateTime());
                $data->setCreateBy($this->getUser()->getId());
                $data->setUpdateBy($this->getUser()->getId());

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_catalog_categories_list'));
            }
        }

        return $this->render('CatalogBundle:Categories:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $id){
        $service = $this->get('cms.catalog.categories.service');

        $form = $service->generateForm($service->findOneBy(['id' => $id]));

        if ($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                /** @var Categories $data */
                $data = $form->getData();

                $data->setDateUpdate(new \DateTime());
                $data->setUpdateBy($this->getUser()->getId());

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_catalog_categories_list'));
            }
        }

        return $this->render('CatalogBundle:Categories:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id){
        $service = $this->get('cms.catalog.categories.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_catalog_categories_list'));
    }
}