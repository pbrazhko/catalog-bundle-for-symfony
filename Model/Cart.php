<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 08.10.16
 * Time: 17:01
 */

namespace CMS\CatalogBundle\Model;


class Cart
{
    /**
     * @var array
     */
    private $products;

    /**
     * Cart constructor.
     */
    public function __construct()
    {
        $this->products = [];
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     *
     * @return Cart
     */
    public function setProducts(array $products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * @param int $id
     * @param float $price
     * @param string $currency
     * @return $this
     */
    public function addProduct(int $id, float $price, string $currency)
    {
        if(!$this->hasProduct($id)){
            $this->products[$id] = new CartProduct($id, 1, $price, $currency);

            return $this;
        }

        $this->products[$id]->setCount($this->products[$id]->getCount() + 1);

        return $this;
    }

    /**
     * @param $id
     * @return CartProduct|null
     */
    public function getProduct($id)
    {
        if(!$this->hasProduct($id)){
            return null;
        }

        return $this->products[$id];
    }

    /**
     * @param int $id
     * @return $this
     */
    public function deleteProduct(int $id)
    {
        if($this->hasProduct($id)) {
            unset($this->products[$id]);
        }

        return $this;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function reduceQuantity(int $id)
    {
        if($this->hasProduct($id) && $this->products[$id]->getCount() > 1){
            $this->products[$id]->setCount($this->products[$id]->getCount() - 1);
        }

        return $this;
    }

    public function increaseQuantity(int $id)
    {
        if($this->hasProduct($id)){
            $this->products[$id]->setCount($this->products[$id]->getCount() + 1);
        }

        return $this;
    }

    /**
     * @param int $id
     * @param int $count
     * @return $this
     */
    public function changeProductCount(int $id, int $count)
    {
        if($this->hasProduct($id)){
            $this->products[$id]->setCount($count);
        }

        return $this;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function hasProduct(int $id)
    {
        return array_key_exists($id, $this->products);
    }

    /**
     * @return int
     */
    public function getCount()
    {
        $count = 0;

        /** @var CartProduct $product */
        foreach ($this->products as $product) {
            $count = $count + $product->getCount();
        }

        return $count;
    }

    /**
     * @return array
     */
    public function getTotal()
    {
        $total = [];

        /** @var CartProduct $product */
        foreach ($this->products as $product) {
            $total = array_merge_recursive($total, [$product->getCurrency() => $product->getTotalPrice()]);

            if(is_array($total[$product->getCurrency()])) {
                $total[$product->getCurrency()] = array_sum($total[$product->getCurrency()]);
            }
        }

        return $total;
    }
}