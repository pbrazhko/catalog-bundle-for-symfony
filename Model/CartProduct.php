<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 09.10.16
 * Time: 16:10
 */

namespace CMS\CatalogBundle\Model;


class CartProduct
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $count;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string;
     */
    private $currency;

    /**
     * CartProduct constructor.
     * @param int $id
     * @param int $count
     * @param float $price
     * @param string $currency
     */
    public function __construct($id, $count, $price, $currency)
    {
        $this->id = $id;
        $this->count = $count;
        $this->price = $price;
        $this->currency = $currency;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return CartProduct
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     *
     * @return CartProduct
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return CartProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return CartProduct
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    public function getTotalPrice()
    {
        return $this->count * $this->price;
    }
}