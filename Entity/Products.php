<?php

namespace CMS\CatalogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;

/**
 * Products
 */
class Products {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;
    
    /**
     * @var float 
     */
    private $purchase_price;
    
    /**
     * @var float 
     */
    private $margin;
    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var \DateTime
     * @JMS\Exclude
     */
    private $date_create;
    
    /**
     * @var \DateTime
     * @JMS\Exclude
     */
    private $date_update;

    /**
     * @var integer
     * @JMS\Exclude
     */
    private $create_by;
    
    /**
     * @var integer
     * @JMS\Exclude
     */
    private $update_by;

    /**
     * @var boolean
     */
    private $is_published;

    /**
     * @var integer 
     */
    private $sort;
    
    /**
     * @var float
     */
    private $rating = '1.0';

    /**
     * @var boolean
     */
    private $is_deleted;

    /**
     * @var ArrayCollection Properties
     */
    private $properties;
    
    /**
     * @var Variations
     */
    private $variations;

    /**
     * @var array
     */
    private $orders;

    public function __construct() {
        $this->properties = new ArrayCollection();
        $this->variations = new ArrayCollection();
    }

    /**
     * Set id
     * 
     * @param integer $id
     * @return Products
     */
    public function setId($id){
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Products
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * Set purchase price
     *
     * @param float $purchase_price
     * @return Products
     */
    public function setPurchasePrice($purchase_price) {
        $this->purchase_price = $purchase_price;

        return $this;
    }

    /**
     * Get purchase price
     *
     * @return float
     */
    public function getPurchasePrice() {
        return $this->purchase_price;
    }
    
    /**
     * Set margin
     *
     * @param float $margin
     * @return Products
     */
    public function setMargin($margin) {
        $this->margin = $margin;

        return $this;
    }

    /**
     * Get margin
     *
     * @return float
     */
    public function getMargin() {
        return $this->margin;
    }
    
    /**
     * Set price
     *
     * @param float $price
     * @return Products
     */
    public function setPrice($price) {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Products
     */
    public function setCurrency($currency) {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency() {
        return $this->currency;
    }

    /**
     * Set date_create
     *
     * @param \DateTime $date_create
     * @return Products
     */
    public function setDateCreate($date_create) {
        $this->date_create = $date_create;

        return $this;
    }
    
    /**
     * Get date_create
     *
     * @return \DateTime
     */
    public function getDateCreate() {
        return $this->date_create;
    }
    
    /**
     * Set date_update
     *
     * @param \DateTime $date_update
     * @return Products
     */
    public function setDateUpdate($date_update) {
        $this->date_update = $date_update;

        return $this;
    }
    
    /**
     * Get date_update
     *
     * @return \DateTime
     */
    public function getDateUpdate() {
        return $this->date_update;
    }
    
    /**
     * Set create_by
     *
     * @param integer $create_by
     * @return Products
     */
    public function setCreateBy($create_by) {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * Get create_by
     *
     * @return integer
     */
    public function getCreateBy() {
        return $this->create_by;
    }
    
    /**
     * Set update_by
     *
     * @param integer $update_by
     * @return Products
     */
    public function setUpdateBy($update_by) {
        $this->update_by = $update_by;

        return $this;
    }

    /**
     * Get update_by
     *
     * @return integer
     */
    public function getUpdateBy() {
        return $this->update_by;
    }

    /**
     * Set is_published
     *
     * @param boolean $is_published
     * @return Products
     */
    public function setIsPublished($is_published) {
        $this->is_published = $is_published;

        return $this;
    }

    /**
     * Get is_published
     *
     * @return boolean
     */
    public function getIsPublished() {
        return $this->is_published;
    }

    /**
     * Set is_deleted
     *
     * @param boolean $is_deleted
     * @return Products
     */
    public function setIsDeleted($is_deleted) {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * Get is_deleted
     *
     * @return boolean
     */
    public function getIsDeleted() {
        return $this->is_deleted;
    }
    
    /**
     * Set sort
     * 
     * @param integer $sort
     * @return Products
     */
    public function setSort($sort){
        $this->sort = $sort;
        
        return $this;
    }
    
    /**
     * Get sort
     * 
     * @return integer
     */
    public function getSort(){
        return $this->sort;
    }
    
    /**
     * Set rating
     * 
     * @param float $rating
     * @return Products
     */
    public function setRating($rating){
        $this->rating = $rating;
        
        return $this;
    }
    
    /**
     * Get rating
     * 
     * @return float
     */
    public function getRating(){
        return $this->rating;
    }

    /**
     * Set properties
     * 
     * @param ArrayCollection $properties
     * @return Properties
     */
    public function setProperties($properties) {
        $this->properties = $properties;

        return $this;
    }

    /**
     * Get properties
     * 
     * @return ArrayCollection
     */
    public function getProperties() {
        return $this->properties;
    }

    /**
     * 
     * @param Properties $properties
     */
    public function removeProperty($properties) {
        $this->properties->removeElement($properties);
    }
    
    /**
     * @return Variations
     */
    public function getVariations()
    {
        return $this->variations;
    }

    /**
     * @param Variations $variations
     *
     * @return Products
     */
    public function setVariations($variations)
    {
        $this->variations = $variations;
        
        return $this;
    }

    /**
     * @return array
     */
    public function getOrders(): array
    {
        return $this->orders;
    }

    /**
     * @param array $orders
     * @return $this
     */
    public function setOrders(array $orders)
    {
        $this->orders = $orders;

        return $this;
    }
}
