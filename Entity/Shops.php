<?php

namespace CMS\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shops
 */
class Shops
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $addressRegistration;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Shops
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Shops
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set addressRegistration
     *
     * @param string $addressRegistration
     * @return Shops
     */
    public function setAddressRegistration($addressRegistration)
    {
        $this->addressRegistration = $addressRegistration;

        return $this;
    }

    /**
     * Get addressRegistration
     *
     * @return string 
     */
    public function getAddressRegistration()
    {
        return $this->addressRegistration;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Shops
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Shops
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set date_create
     *
     * @param \DateTime $date_create
     * @return Shops
     */
    public function setDateCreate($date_create)
    {
        $this->date_create = new \DateTime();

        return $this;
    }

    /**
     * Get date_create
     *
     * @return \DateTime 
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * Set date_update
     *
     * @param \DateTime $date_update
     * @return Shops
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = new \DateTime();

        return $this;
    }

    /**
     * Get date_update
     *
     * @return \DateTime 
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * Set create_by
     *
     * @param integer $create_by
     * @return Shops
     */
    public function setCreateBy($create_by)
    {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * Get create_by
     *
     * @return integer 
     */
    public function getCreateBy()
    {
        return $this->create_by;
    }

    /**
     * Set update_by
     *
     * @param integer $update_by
     * @return Shops
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;

        return $this;
    }

    /**
     * Get update_by
     *
     * @return integer 
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }
}
