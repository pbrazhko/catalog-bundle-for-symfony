<?php

namespace CMS\CatalogBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Variations
 */
class Variations
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var Categories
     */
    private $category;

    /**
     * @var Variations
     */
    private $related_variations;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $keywords;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $introduction;

    /**
     * @var string
     */
    private $content;
    
    /**
     * @var ArrayCollection Images
     */
    private $images;

    /**
     * @var ArrayCollection
     */
    private $products;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    /**
     * @var boolean
     */
    private $is_published;

    /**
     * @var integer
     */
    private $sort;

    /**
     * @var float
     */
    private $rating = '1.0';

    /**
     * @var boolean
     */
    private $is_deleted;

    /**
     * @var ArrayCollection Comments
     */
    private $comments;
    
    /**
     * @var array
     */
    private $delivery_methods;

    /**
     * @var array
     */
    private $payment_types;

    /**
     *
     * @var ArrayCollection
     */
    private $orders_list;

    public function __construct() {
        $this->delivery_methods = new ArrayCollection();
        $this->related_variations = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Products
     */
    public function setId($id){
        $this->id = $id;

        return $this;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param integer $category
     * @return Products
     */
    public function setCategory($category) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return integer
     */
    public function getCategory() {
        return $this->category;
    }
    
    /**
     * Set related products
     *
     * @param ArrayCollection $related_variations
     * @return Products
     */
    public function setRelatedVariations($related_variations){
        $this->related_variations = $related_variations;

        return $this;
    }

    /**
     * Get related products
     *
     * @return ArrayCollection
     */
    public function getRelatedVariations(){
        return $this->related_variations;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Products
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Products
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Products
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set introduction
     *
     * @param string $introduction
     * @return Products
     */
    public function setIntroduction($introduction) {
        $this->introduction = $introduction;

        return $this;
    }

    /**
     * Get introduction
     *
     * @return string
     */
    public function getIntroduction() {
        return $this->introduction;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Products
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }
    
    /**
     * Set date_create
     *
     * @param \DateTime $date_create
     * @return Products
     */
    public function setDateCreate($date_create) {
        $this->date_create = $date_create;

        return $this;
    }

    /**
     * Get date_create
     *
     * @return \DateTime
     */
    public function getDateCreate() {
        return $this->date_create;
    }

    /**
     * Set date_update
     *
     * @param \DateTime $date_update
     * @return Products
     */
    public function setDateUpdate($date_update) {
        $this->date_update = $date_update;

        return $this;
    }

    /**
     * Get date_update
     *
     * @return \DateTime
     */
    public function getDateUpdate() {
        return $this->date_update;
    }

    /**
     * set images
     *
     * @param ArrayCollection $images
     * @return Products
     */
    public function setImages($images) {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return ArrayCollection
     */
    public function getImages() {
        return $this->images;
    }

    /**
     * Set create_by
     *
     * @param integer $create_by
     * @return Products
     */
    public function setCreateBy($create_by) {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * Get create_by
     *
     * @return integer
     */
    public function getCreateBy() {
        return $this->create_by;
    }

    /**
     * Set update_by
     *
     * @param integer $update_by
     * @return Products
     */
    public function setUpdateBy($update_by) {
        $this->update_by = $update_by;

        return $this;
    }

    /**
     * Get update_by
     *
     * @return integer
     */
    public function getUpdateBy() {
        return $this->update_by;
    }

    /**
     * Set is_published
     *
     * @param boolean $is_published
     * @return Products
     */
    public function setIsPublished($is_published) {
        $this->is_published = $is_published;

        return $this;
    }

    /**
     * Get is_published
     *
     * @return boolean
     */
    public function getIsPublished() {
        return $this->is_published;
    }

    /**
     * Set is_deleted
     *
     * @param boolean $is_deleted
     * @return Products
     */
    public function setIsDeleted($is_deleted) {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * Get is_deleted
     *
     * @return boolean
     */
    public function getIsDeleted() {
        return $this->is_deleted;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return Products
     */
    public function setSort($sort){
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort(){
        return $this->sort;
    }

    /**
     * Set rating
     *
     * @param float $rating
     * @return Products
     */
    public function setRating($rating){
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return float
     */
    public function getRating(){
        return $this->rating;
    }

    /**
     *
     * @param Properties $properties
     */
    public function removeProperty($properties) {
        $this->properties->removeElement($properties);
    }

    /**
     * Set comments
     *
     * @param ArrayCollection $comments
     * @return Products
     */
    public function setComments($comments){
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return ArrayCollection
     */
    public function getComments(){
        return $this->comments;
    }

    /**
     * @return int
     */
    public function getDeliveryMethods()
    {
        return $this->delivery_methods;
    }
    
    /**
     * @param int $delivery_methods
     * @return $this
     */
    public function setDeliveryMethods($delivery_methods)
    {
        $this->delivery_methods = $delivery_methods;

        return $this;
    }

    /**
     * @param $delivery_method
     * @return $this
     */
    public function addDeliveryMethods($delivery_method)
    {
        $this->delivery_methods[] = $delivery_method;

        return $this;
    }

    /**
     * @return array
     */
    public function getPaymentTypes()
    {
        return $this->payment_types;
    }

    /**
     * @param array $payment_types
     *
     * @return Variations
     */
    public function setPaymentTypes($payment_types)
    {
        $this->payment_types = $payment_types;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param ArrayCollection $products
     *
     * @return Variations
     */
    public function setProducts($products)
    {
        $this->products = $products;
        
        return $this;
    }
}

