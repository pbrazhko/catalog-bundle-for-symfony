<?php

namespace CMS\CatalogBundle\Entity;

/**
 * DeliveryMethods
 */
class DeliveryMethods
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var float
     */
    private $price;

    /**
     * @var Currencies
     */
    private $currency;

    /**
     * @var integer
     */
    private $time_delivery;

    /**
     * @var integer
     */
    private $max_time_delivery;

    /**
     * @var bool
     */
    private $address_required = true;

    /**
     * @var boolean
     */
    private $is_published;

    /**
     * @var boolean
     */
    private $is_deleted;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return DeliveryMethods
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return DeliveryMethods
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return Currencies
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param Currencies $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return int
     */
    public function getTimeDelivery()
    {
        return $this->time_delivery;
    }

    /**
     * @param int $time_delivery
     * @return $this
     */
    public function setTimeDelivery(int $time_delivery)
    {
        $this->time_delivery = $time_delivery;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaxTimeDelivery()
    {
        return $this->max_time_delivery;
    }

    /**
     * @param mixed $max_time_delivery
     * @return $this
     */
    public function setMaxTimeDelivery($max_time_delivery)
    {
        $this->max_time_delivery = $max_time_delivery;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isAddressRequired(): bool
    {
        return $this->address_required;
    }

    /**
     * @param boolean $address_required
     * @return DeliveryMethods
     */
    public function setAddressRequired(bool $address_required)
    {
        $this->address_required = $address_required;

        return $this;
    }


    /**
     * @param boolean $is_published
     * @return $this
     */
    public function setIsPublished($is_published)
    {
        $this->is_published = $is_published;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->is_published;
    }


    /**
     * @param boolean $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set create_by
     *
     * @param integer $create_by
     * @return Categories
     */
    public function setCreateBy($create_by) {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * Get createBy
     *
     * @return integer
     */
    public function getCreateBy() {
        return $this->create_by;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     * @return $this
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;

        return $this;
    }

    /**
     * Set date_create
     *
     * @param \DateTime $date_create
     * @return Categories
     */
    public function setDateCreate($date_create) {
        $this->date_create = new \DateTime();

        return $this;
    }

    /**
     * Get date_create
     *
     * @return \DateTime
     */
    public function getDateCreate() {
        return $this->date_create;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     * @return $this
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = new \DateTime();

        return $this;
    }
}

