<?php

namespace CMS\CatalogBundle\Entity;

/**
 * PaymentType
 */
class PaymentTypes
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $paymentService;

    /**
     * @var boolean
     */
    private $is_deleted = false;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return PaymentTypes
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentService()
    {
        return $this->paymentService;
    }

    /**
     * @param string $paymentService
     *
     * @return PaymentTypes
     */
    public function setPaymentService($paymentService)
    {
        $this->paymentService = $paymentService;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     *
     * @return PaymentTypes
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * @param \DateTime $date_create
     *
     * @return PaymentTypes
     */
    public function setDateCreate($date_create)
    {
        $this->date_create = $date_create;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     *
     * @return PaymentTypes
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = $date_update;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreateBy()
    {
        return $this->create_by;
    }

    /**
     * @param int $create_by
     *
     * @return PaymentTypes
     */
    public function setCreateBy($create_by)
    {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     *
     * @return PaymentTypes
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;

        return $this;
    }
}

