<?php

namespace CMS\CatalogBundle\Entity;

/**
 * Order
 */
class Orders
{
    const
        STATUS_CANCELED = 0,
        STATUS_NEW = 1,
        STATUS_IN_PROGRESS = 2,
        STATUS_READY = 3,
        STATUS_IN_DELIVERY = 4,
        STATUS_COMPLETE = 5;
    
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $customer;

    /**
     * @var string
     */
    private $customerPhone;

    /**
     * @var string
     */
    private $customerEmail;

    /**
     * @var string
     */
    private $deliveryAddress;

    /**
     * @var array
     */
    private $products;

    /**
     * @var DeliveryMethods
     */
    private $deliveryMethod;

    /**
     * @var PaymentTypes
     */
    private $paymentType;

    /**
     * @var float
     */
    private $totalPrice;

    /**
     * @var Currencies
     */
    private $currency;

    /**
     * @var int
     */
    private $status;

    /**
     * @var boolean
     */
    private $is_deleted;

    /**
     * @var integer
     */
    private $update_by;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Orders
     */
    public function setNumber(string $number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomer(): string
    {
        return $this->customer;
    }

    /**
     * @param string $customer
     * @return $this
     */
    public function setCustomer(string $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerPhone(): string
    {
        return $this->customerPhone;
    }

    /**
     * @param string $customerPhone
     * @return $this
     */
    public function setCustomerPhone(string $customerPhone)
    {
        $this->customerPhone = $customerPhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @param string $customerEmail
     * @return $this
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @param string $deliveryAddress
     * @return $this
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     * @return $this
     */
    public function setProducts(array $products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * @param OrdersProducts $product
     * @return $this
     */
    public function addProduct(OrdersProducts $product)
    {
        $product->setOrder($this);
        
        $this->products[] = $product;
        
        return $this;
    }

    /**
     * @return DeliveryMethods
     */
    public function getDeliveryMethod(): DeliveryMethods
    {
        return $this->deliveryMethod;
    }

    /**
     * @param DeliveryMethods $deliveryMethod
     * @return $this
     */
    public function setDeliveryMethod(DeliveryMethods $deliveryMethod)
    {
        $this->deliveryMethod = $deliveryMethod;

        return $this;
    }

    /**
     * @return PaymentTypes
     */
    public function getPaymentType(): PaymentTypes
    {
        return $this->paymentType;
    }

    /**
     * @param PaymentTypes $paymentType
     * @return $this
     */
    public function setPaymentType(PaymentTypes $paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     * @return $this
     */
    public function setTotalPrice(float $totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * @return Currencies
     */
    public function getCurrency(): Currencies
    {
        return $this->currency;
    }

    /**
     * @param Currencies $currency
     * @return $this
     */
    public function setCurrency(Currencies $currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Orders
     */
    public function setStatus(int $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted(): bool
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return Orders
     */
    public function setIsDeleted(bool $is_deleted)
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     * @return Orders
     */
    public function setUpdateBy(int $update_by)
    {
        $this->update_by = $update_by;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreate(): \DateTime
    {
        return $this->date_create;
    }

    /**
     * @param \DateTime $date_create
     * @return Orders
     */
    public function setDateCreate(\DateTime $date_create)
    {
        $this->date_create = $date_create;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate(): \DateTime
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     * @return Orders
     */
    public function setDateUpdate(\DateTime $date_update)
    {
        $this->date_update = $date_update;

        return $this;
    }
}

