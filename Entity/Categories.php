<?php

namespace CMS\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Categories
 */
class Categories {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $parent;

    /**
     * @var ArrayCollection
     */
    private $childs;

    /**
     * @var int
     */
    private $shop;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $keywords;

    /**
     * @var string
     */
    private $description;

    /**
     * @var ArrayCollection
     */
    private $images;

    /**
     * @var boolean
     */
    private $is_published;

    /**
     * @var boolean
     */
    private $is_deleted;

    /**
     * @var integer
     */
    private $sort;

    /**
     * @var ArrayCollection
     */
    private $variations;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    public function __construct() {
        $this->variations = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set parentCategory
     *
     * @param integer $parent
     * @return Categories
     */
    public function setParent($parent) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return integer
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * @param ArrayCollection $childs
     * @return $this
     */
    public function setChilds($childs)
    {
        $this->childs = $childs;

        return $this;
    }

    /**
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int $shop
     * @return $this
     */
    public function setShop($shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Categories
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Categories
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Categories
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set images
     *
     * @param ArrayCollection $images
     * @return Categories
     */
    public function setImages($images) {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return string
     */
    public function getImages() {
        return $this->images;
    }

    /**
     * Set is_published
     *
     * @param boolean $is_published
     * @return Categories
     */
    public function setIsPublished($is_published) {
        $this->is_published = $is_published;

        return $this;
    }

    /**
     * Get is_published
     *
     * @return boolean
     */
    public function getIsPublished() {
        return $this->is_published;
    }

    /**
     * Set is_deleted
     *
     * @param boolean $is_deleted
     * @return Categories
     */
    public function setIsDeleted($is_deleted) {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * Get is_deleted
     *
     * @return boolean
     */
    public function getIsDeleted() {
        return $this->is_deleted;
    }
    
    /**
     * Set sort
     * 
     * @param integer $sort
     * @return Categories
     */
    public function setSort($sort){
        $this->sort = $sort;
        
        return $this;
    }
    
    /**
     * Get sort
     * 
     * @return integer
     */
    public function getSort(){
        return $this->sort;
    }

    /**
     * Set products
     *
     * @param ArrayCollection $variations
     * @return Categories
     */
    public function setVariations($variations) {
        $this->variations = $variations;

        return $this;
    }

    /**
     * Get products
     *
     * @return ArrayCollection
     */
    public function getVariations() {
        return $this->variations;
    }

    /**
     * Set create_by
     *
     * @param integer $create_by
     * @return Categories
     */
    public function setCreateBy($create_by) {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * Get createBy
     *
     * @return integer
     */
    public function getCreateBy() {
        return $this->create_by;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     * @return $this
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;

        return $this;
    }

    /**
     * Set date_create
     *
     * @param \DateTime $date_create
     * @return Categories
     */
    public function setDateCreate($date_create) {
        $this->date_create = new \DateTime();

        return $this;
    }

    /**
     * Get date_create
     *
     * @return \DateTime
     */
    public function getDateCreate() {
        return $this->date_create;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     * @return $this
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = new \DateTime();

        return $this;
    }
}
