<?php

namespace CMS\CatalogBundle\Entity;

/**
 * OrdersProducts
 */
class OrdersProducts
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Orders
     */
    private $order;

    /**
     * @var Products
     */
    private $product;

    /**
     * @var int
     */
    private $count;

    /**
     * @var float
     */
    private $price;

    /**
     * @var Currencies
     */
    private $currency;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Orders
     */
    public function getOrder(): Orders
    {
        return $this->order;
    }

    /**
     * @param Orders $order
     * @return OrdersProducts
     */
    public function setOrder(Orders $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return Products
     */
    public function getProduct(): Products
    {
        return $this->product;
    }

    /**
     * @param Products $product
     * @return OrdersProducts
     */
    public function setProduct(Products $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return OrdersProducts
     */
    public function setCount(int $count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return OrdersProducts
     */
    public function setPrice(float $price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Currencies
     */
    public function getCurrency(): Currencies
    {
        return $this->currency;
    }

    /**
     * @param Currencies $currency
     * @return OrdersProducts
     */
    public function setCurrency(Currencies $currency)
    {
        $this->currency = $currency;

        return $this;
    }
}

