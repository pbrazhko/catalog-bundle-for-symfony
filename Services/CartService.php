<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 08.10.16
 * Time: 16:46
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\Entity\Products;
use CMS\CatalogBundle\Exceptions\InvalidArgumentException;
use CMS\CatalogBundle\Model\Cart;
use Symfony\Component\HttpFoundation\Session\Session;

class CartService
{
    const CART_SESSION_NAME = 'cart';

    /**
     * @var Session
     */
    private $storage;

    /**
     * @var ProductsService
     */
    private $productsService;

    /**
     * CartService constructor.
     * @param ProductsService $productsService
     */
    public function __construct(ProductsService $productsService)
    {
        $this->storage = new Session();
        $this->productsService = $productsService;
    }

    /**
     * @param $id
     * @return $this
     */
    public function addProduct($id)
    {
        /** @var Products $product */
        if(null === ($product = $this->productsService->findOneBy(['id' => $id]))){
            throw new InvalidArgumentException(sprintf('Product of id %s not found'));
        }

        $cart = $this
            ->loadCart()
            ->addProduct(
                $product->getId(),
                $product->getPrice(),
                $product->getCurrency()->getCode()
            );

        $this->saveCart($cart);

        return $this;
    }

    public function deleteProduct($id)
    {
        /** @var Products $product */
        if(null === ($product = $this->productsService->findOneBy(['id' => $id]))){
            throw new InvalidArgumentException(sprintf('Product of id %s not found'));
        }

        $cart = $this
            ->loadCart()
            ->deleteProduct(
                $product->getId()
            );

        $this->saveCart($cart);

        return $this;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function reduceQuantity(int $id)
    {
        /** @var Products $product */
        if(null === ($product = $this->productsService->findOneBy(['id' => $id]))){
            throw new InvalidArgumentException(sprintf('Product of id %s not found', $id));
        }

        $cart = $this
            ->loadCart()
            ->reduceQuantity(
                $product->getId()
            );

        $this->saveCart($cart);

        return $this;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function increaseQuantity(int $id)
    {
        /** @var Products $product */
        if(null === ($product = $this->productsService->findOneBy(['id' => $id]))){
            throw new InvalidArgumentException(sprintf('Product of id %s not found'));
        }

        $cart = $this
            ->loadCart()
            ->increaseQuantity(
                $product->getId()
            );

        $this->saveCart($cart);

        return $this;
    }

    /**
     * @param int $id
     * @param int $count
     * @return $this
     */
    public function changeProductCount(int $id, int $count)
    {
        /** @var Products $product */
        if(null === ($product = $this->productsService->findOneBy(['id' => $id]))){
            throw new InvalidArgumentException(sprintf('Product of id %s not found'));
        }

        $cart = $this
            ->loadCart()
            ->changeProductCount(
                $product->getId(),
                $count
            );

        $this->saveCart($cart);

        return $this;
    }

    /**
     * @return Cart
     */
    public function loadCart()
    {
        return unserialize($this->storage->get(CartService::CART_SESSION_NAME, serialize(new Cart())));
    }

    /**
     * @param Cart $cart
     */
    public function saveCart(Cart $cart)
    {
        $this->storage->set(CartService::CART_SESSION_NAME, serialize($cart));
    }

    public function clearCart()
    {
        $this->storage->set(CartService::CART_SESSION_NAME, serialize(new Cart()));
    }
}