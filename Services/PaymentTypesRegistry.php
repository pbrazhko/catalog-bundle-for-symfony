<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 25.09.16
 * Time: 15:38
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\PaymentTypeInterface;

class PaymentTypesRegistry
{
    /**
     * @var array
     */
    private $paymentTypes;

    /**
     * @return array
     */
    public function getPaymentTypes()
    {
        return $this->paymentTypes;
    }

    /**
     * @param $paymentTypeName
     * @param PaymentTypeInterface $paymentType
     * @return $this
     */
    public function addPaymentType($paymentTypeName, PaymentTypeInterface $paymentType){
        $this->paymentTypes[$paymentTypeName] = $paymentType;

        return $this;
    }

    /**
     * @param array $paymentTypes
     *
     * @return PaymentTypesRegistry
     */
    public function setPaymentTypes($paymentTypes)
    {
        $this->paymentTypes = $paymentTypes;

        return $this;
    }
}