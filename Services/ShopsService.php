<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 16.12.14
 * Time: 13:13
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\Form\ShopsType;
use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class ShopsService extends AbstractCoreService{

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'CatalogBundle:Shops';
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            ShopsType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }
}