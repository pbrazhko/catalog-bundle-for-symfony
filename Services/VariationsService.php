<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 01.07.16
 * Time: 17:39
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\Form\VariationsType;
use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormFactory;

class VariationsService extends AbstractCoreService
{
    /**
     * @param string $query
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return mixed
     */
    public function match(string $query, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->match($query, $orderBy, $limit, $offset);
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return [];
    }

    /**
     * @param FormFactory $form
     * @param null $data
     * @return \Symfony\Component\Form\FormBuilderInterface
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            VariationsType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * @return string
     */
    public function getRepositoryName()
    {
        return 'CatalogBundle:Variations';
    }
}