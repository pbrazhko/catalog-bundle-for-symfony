<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.04.15
 * Time: 17:45
 */

namespace CMS\CatalogBundle\Services;

use CMS\CatalogBundle\Form\CategoriesType;
use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class CategoriesService extends AbstractCoreService{

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array(
            'is_published' => true,
            'is_deleted' => false
        );
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            CategoriesType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'CatalogBundle:Categories';
    }
}