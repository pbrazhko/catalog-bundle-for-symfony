<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 30.10.16
 * Time: 16:51
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\Entity\Orders;
use CMS\CatalogBundle\Form\OrdersType;
use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormFactory;

class OrdersService extends AbstractCoreService
{
    public function sendMailOrder(Orders $order)
    {
        $logger = $this->container->get('monolog.logger.php');

        $message = \Swift_Message::newInstance();

        $message
            ->setSubject($this->container->get('translator')->trans('email.order.subject', [], 'catalog'))
            ->setFrom($this->container->getParameter('mailer_user'))
            ->setTo([$order->getCustomerEmail()])
            ->setBcc(['info@constructor-plus.ru'])
            ->setBody(
                $this->container->get('templating')->render($this->container->getParameter('catalog.email_templates.order'), [
                    'order' => $order
                ]),
                'text/html'
            );

        return $this->container->get('mailer')->send($message);
    }

    public function getDefaultsCriteria()
    {
        return [
            'is_deleted' => false
        ];
    }

    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            OrdersType::class,
            $data,
            [
                'data_class' => $this->getRepositoryClass()
            ]
        );
    }

    public function getRepositoryName()
    {
        return 'CatalogBundle:Orders';
    }
}