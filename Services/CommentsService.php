<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 22.04.15
 * Time: 10:00
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\Form\CommentsType;
use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class CommentsService extends AbstractCoreService{
    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new CommentsType(),
            $data,
            array(
                'data_class' => $this->getRepositoryName()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'CatalogBundle:Comments';
    }

}