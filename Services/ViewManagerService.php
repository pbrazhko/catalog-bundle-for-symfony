<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 04.09.16
 * Time: 17:34
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\Exceptions\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class ViewManagerService extends \ArrayIterator
{
    const VIEW_SESSION_NAME = 'view_name_session';

    /**
     * @var string
     */
    private $name;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var string
     */
    private $currentView;

    /**
     * @var boolean
     */
    private $ajaxRequest = false;

    /**
     * @var int
     */
    private $limit = 20;

    /**
     * @var int
     */
    private $offset = 0;

    /**
     * @var int
     */
    private $count = 0;

    /**
     * @var array
     */
    private $supportedViewsType;

    /**
     * @var string
     */
    private $defaultView;

    /**
     * ViewManagerService constructor.
     * @param string $name
     * @param Session $session
     * @param int $supportedViewsType
     * @param \Twig_Environment $twig
     */
    public function __construct(string $name, Session $session, $supportedViewsType, \Twig_Environment $twig)
    {
        $this->name = $name;
        $this->session = $session;
        $this->supportedViewsType = $supportedViewsType;
        $this->twig = $twig;

        parent::__construct($this->supportedViewsType);
    }

    /**
     * @return string
     */
    public function getCurrentView()
    {
        if(null === $this->currentView){
            $this->currentView = $this->session->get(
                ViewManagerService::VIEW_SESSION_NAME . ':' . $this->name,
                $this->getDefaultView()
            );
        }

        return $this->currentView;
    }

    /**
     * @param string $currentView
     *
     * @return ViewManagerService
     */
    public function setCurrentView($currentView)
    {
        if(!isset($this->supportedViewsType[$currentView])){
            throw new InvalidArgumentException(
                sprintf('Type %s is not supported', $currentView)
            );
        }

        $this->currentView = $currentView;

        return $this->saveCurrentView();
    }

    /**
     * @return boolean
     */
    public function isAjaxRequest()
    {
        return $this->ajaxRequest;
    }

    /**
     * @param boolean $ajaxRequest
     *
     * @return ViewManagerService
     */
    public function setAjaxRequest($ajaxRequest)
    {
        $this->ajaxRequest = $ajaxRequest;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     *
     * @return ViewManagerService
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     *
     * @return ViewManagerService
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     *
     * @return ViewManagerService
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @param $view
     * @return string
     */
    public function getTemplate($view){
        return $this->setCurrentView($view)->supportedViewsType[$this->getCurrentView()];
    }

    /**
     * @param $view
     * @param array $context
     * @return Response
     */
    public function render($view, array $context){
        $this->setCurrentView($view);

        $template = $this->getTemplate($view);

        if($this->ajaxRequest){
            return $this->renderBlock($template, $context);
        }

        return new Response($this->twig->render($template, $context));
    }

    /**
     * @param $template
     * @param $context
     * @return Response
     */
    private function renderBlock($template, array $context){
        return new Response($this->twig->loadTemplate($template)->displayBlock($this->getCurrentView(), $context));
    }

    /**
     * @return $this
     */
    private function saveCurrentView()
    {
        $this->session->set(
            ViewManagerService::VIEW_SESSION_NAME . ':' . $this->name,
            $this->currentView
        );

        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultView()
    {
        return $this->defaultView;
    }

    /**
     * @param string $defaultView
     *
     * @return ViewManagerService
     */
    public function setDefaultView($defaultView)
    {
        $this->defaultView = $defaultView;

        return $this;
    }
}