<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 25.04.2015
 * Time: 12:58
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\Form\DeliveryMethodsType;
use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class DeliveryMethodsService extends AbstractCoreService{
    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            DeliveryMethodsType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'CatalogBundle:DeliveryMethods';
    }

}