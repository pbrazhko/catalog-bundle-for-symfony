<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.04.15
 * Time: 11:12
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\Form\ProductsType;
use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class ProductsService extends AbstractCoreService
{
    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array(
            'is_deleted' => false
        );
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            ProductsType::class,
            $data,
            array(
                'data_class' =>$this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'CatalogBundle:Products';
    }

}