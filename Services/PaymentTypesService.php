<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 25.09.16
 * Time: 15:58
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\Form\PaymentTypesType;
use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormFactory;

class PaymentTypesService extends AbstractCoreService
{
    public function getDefaultsCriteria()
    {
        return [];
    }

    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            PaymentTypesType::class,
            $data,
            [
                'data_class' => $this->getRepositoryClass()
            ]
        );
    }

    public function getRepositoryName()
    {
        return 'CatalogBundle:PaymentTypes';
    }
}