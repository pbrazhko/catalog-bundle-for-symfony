<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 11.12.16
 * Time: 15:11
 */

namespace CMS\CatalogBundle\Services;


use CMS\CatalogBundle\Entity\Favorites;
use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class FavoritesService extends AbstractCoreService
{
    private $favorites;

    /**
     * @param int $userId
     * @param int $variationId
     * @return mixed
     */
    public function addFavorite(int $userId, int $variationId)
    {
        $favorite = (new Favorites())
            ->setUserId($userId)
            ->setVariationId($variationId)
        ;

        return $this->create($favorite);
    }

    /**
     * @param int $userId
     * @param int $variationId
     * @return bool|mixed
     */
    public function removeFavorite(int $userId, int $variationId)
    {
        if(null === ($favorite = $this->findOneBy(['userId' => $userId, 'variationId' => $variationId]))){
            return true;
        }

        return $this->delete($favorite->getId());
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getFavoritesOfUser(int $userId)
    {
        return $this->findBy(['userId' => $userId]);
    }

    /**
     * @param int $userId
     * @return mixed
     */
    public function getFavorites(int $userId)
    {
        if(!isset($this->favorites[$userId])){
            $this->favorites[$userId] = $this->findBy(['userId' => $userId]);
        }

        return $this->favorites[$userId];
    }

    public function getDefaultsCriteria()
    {
        return [];
    }

    public function configureForm(FormFactory $form, $data = null)
    {
        // TODO: Implement configureForm() method.
    }

    public function getRepositoryName()
    {
        return 'CatalogBundle:Favorites';
    }
}