<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 29.01.16
 * Time: 13:13
 */

namespace CMS\CatalogBundle\Provider;

use CMS\CatalogBundle\Services\VariationsService;
use CMS\SearchBundle\Interfaces\SearchProviderInterface;
use CMS\SearchBundle\Services\SearchService;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

class SearchVariationsProvider implements SearchProviderInterface
{
    /**
     * @var VariationsService
     */
    private $variationsService;

    /**
     * SearchProductsProvider constructor
     * @param VariationsService $variationsService
     */
    public function __construct(VariationsService $variationsService)
    {
        $this->variationsService = $variationsService;
    }

    public function buildFilterForm(FormBuilderInterface $builder, array $defaultFieldsData = array())
    {
        $builder
            ->add('title', TextType::class, [
                'required' => false,
                'translation_domain' => 'search'
            ]);
    }

    /**
     * @param Form $form
     * @param SearchService $searchService
     * @return mixed
     */
    public function search(Form $form, SearchService $searchService)
    {
        $result  = [];

        if(null !== ($title = $this->getFormData($form, 'title'))){
            $result = $this->variationsService->match(
                $title,
                $searchService->getOrderBy(),
                $searchService->getLimit()
            );
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'products';
    }

    /**
     * @param Form $form
     * @param $field
     * @param null $default
     * @return mixed|null
     */
    private function getFormData(Form $form, $field, $default = null)
    {
        if ($form->has($field) && !$form->get($field)->isEmpty()) {
            return $form->get($field)->getData();
        }

        return $default;
    }
}