<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 25.09.16
 * Time: 15:54
 */

namespace CMS\CatalogBundle\Provider;


use CMS\CatalogBundle\PaymentTypeInterface;

class CashPaymentTypeProvider implements PaymentTypeInterface
{
    public function getName()
    {
        return 'Cash payment service';
    }
}