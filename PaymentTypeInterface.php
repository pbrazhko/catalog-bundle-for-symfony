<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 25.09.16
 * Time: 15:40
 */

namespace CMS\CatalogBundle;


interface PaymentTypeInterface
{
    /**
     * Return type name
     *
     * @return string
     */
    public function getName();
}